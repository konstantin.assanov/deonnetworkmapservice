# The new network parameters distribution

## Introduction. "Happy path" scenario

1. Adding Notary to the Network forces the Network Map Service to change its network parameters immediately.

2. These network parameters update proposal is distributed by Network Map Service to all nodes of the network.

3. Also Network Map Service informs all the nodes on the *flag* date, before which the network parameters
update should be explicitly accepted by each node.<br>
The *flag* date is defined by Network Map Service parameter *param-update-delay*
```
(*flag* date = time of network parameters update + *param-update-delay*).
```

4. The Node Operator is informed about this network parameters proposal, by CordaRPCops feed.

5. The Node Operator accepts the network parameters update either through CordaRPCops interface,
or via Corda shell with the *run acceptNewNetworkParameters parametersHash: "<new-network-parameters-hash>"* command.

6. The Administrator of Network Map Service can check, which nodes have accepted the network parameters update,
with the */network-map/acknowledgements* request.

7. Once the Node Operator has accepted the network parameters update, it is saved to the **network-parameters-update** file.

8. On the *flag* date each Node will shut down automatically.
Then the Corda node relaunch happens (for example, automatically), and, if the network parameters update has been accepted by the Node,
the network parameters are initialized from the **network-parameters-update** file automatically, thus
the old network parameters are replaced automaticaly with the new ones.

To sum up, if the node accepted the new network parameters, the node is shut down automatically on the *flag* date, and on its restart it is initialized
automatically with the new network parameters update.

9. If the Node did not accept the new network parameters, it's shut down automatically on the *flag* date, and the restart of the node
is possible if and only if the **network-parameters** file is removed manually.



##  Node behavior on network parameters update

1. Once a new notary node is registered, the new network parameters are constructed.
The network map service starts to advertise it through network map object which is loaded
by each node with /network-map "get" polling request.

2. If there is a parameters update advertised and received, the Node executes the /network-map/network-parameters/{hash} request
to download the proposed set of parameters.

3. On reception fo the new network parameters:

    3.1. If the parameters may be accepted automatically, then the parameters acceptance is happening automatically;

    3.2. If not, the Operator of the Node can be informed about the new network parameters
    via subscription to the network parameters feed, provided with CordaRPCOps interface.

4. Manual acceptance of the network parameters can be initiated by the node Operator either through CordaRPCOps call,
or via Corda shell with the run flow command:<br>
```
 run acceptNewNetworkParameters parametersHash: "<new-network-parameters-hash>"
```

5. On acceptance of the new network parameters, its object is stored in the file under the **network-parameters-update** name.

6. On the *flag* day, the Corda Node is shut down automatically.

7. If the stored network parameters are outdated, but the new network parameters, corresponding to the the advertised on the moment of
the Corda Node re-launch, are stored in the **network-parameters-update** file, Corda node replaces the **network-parameters**
with the new ones, saved in the **network-parameters-update** file.

## Network Map Service services

8. The network map service has the list of the nodes that have accepted the new parameters update with the 'get' request:
```
    /network-map/acknowledgements
```

*List of acknowledgements per parameters update:*

```
{
    <new network parameters hash>: [
        {
            "nodeKey": <Node Key>,
            "name": <Node Legal Name>,
            "ackTime": <acknowledgement timestamp>
        },
    ],
    ...
}
```

*Example:*
```
{
    "F87B03DFDC69D2B7596A1713F06EBC3AD67684AB9B80A4593D9CDF1D4B980E85": [
        {
            "nodeKey": "E5FEF6401CE084D600BC2BF5814F27871988006D181A8F6AE035780371136BC0",
            "name": "O=Alice, L=Zurich, C=CH",
            "ackTime": 1568392088271
        },
        {
            "nodeKey": "6C852BA8D76E70C7415CAF36CC6A39365A04E1A6083A0B641FDB31D9A624BE94",
            "name": "O=Bum, L=Zurich, C=CH",
            "ackTime": 1568392602498
        }
    ],
    "EE2732071A59BBF83880803877B36C03C3DB8956C72C7DDEF55DF0AE98237747": [
        {
            "nodeKey": "E5FEF6401CE084D600BC2BF5814F27871988006D181A8F6AE035780371136BC0",
            "name": "O=Alice, L=Zurich, C=CH",
            "ackTime": 1568835130214
        }
    ]
}
```

9. The latest "network parameters update Hash" and the "network parameters update delay" can be consulted with the 'get' request:
```
    /admin/api/params
```
*Example:*
```json
{
    ...,
    "networkParametersUpdateDelay": "PT30M",
    ...
}
```


**Default value** = PT30M

**NB:** before the latest version of network-map-service of the 'doorman' branch, the default value was 10 seconds.

10. The network map service executable is launched with an additional paramter:<br>
*param-update-delay* - delay, given to the nodes to accept the network parameter update.

```
java -Dparam-update-delay=PT30M -jar network-map-service.jar
```
