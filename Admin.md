# Deon Doorman Administrator Usage Guide

## Introduction

This document is a guide for usage of the Deon Network Map Service by Administrator
to manage the Legal identities requests to join the network.

The 'General Deon Doorman' chapter describes the endpoints to be authenticated as Administrator,
get the basic configuration description of the running Network Map Service, and enable the auto-approval mode.

The 'CSR Requests Acceptance' chapter describes the methods to manage the CSR requests.

The 'Registered Nodes management' chapter describes the methods to manage the already working nodes registered at Network Map.

### CSR Request life-cycle

Any CSR Request status to join the Community Zone may be of 2 stages:

**Stage 1.** Initial request state:
* Pending, if the request has been received and will be further processed by Doorman, or
* Invalid, if the request has been considered as invalid (incorrect data, duplicate legal identity, etc.)

**Stage 2.** Resolved request state:
* Authorized, if the request is accepted by Doorman and the needed certificates are issued, or
* Denied, if the request has been rejected by Doorman, no certificate is issued, or
* Expired, if the request has not been processed by Doorman in a due time (see the expiration time).


## 1. General Deon Doorman requests
### 1.1. Login

**POST** http://\<*NMS Address*\>:8080/admin/api/login

Content-Type: application/json

JSON Body:
```json
{
    "user": "<username>",
    "password": "<password>"
}
```

***Returns:*** Authentication token to be used for Bearer Authorization
for all other Deon Doorman Administrator endpoints

### 1.2. Get Deon Doorman configuration parameters

**GET** http://\<*NMS Address*\>:8080/admin/api/params

***Returns:***
```json
{
    "doormanControlled": <is the Deon Doorman activated (true/false)>,
    "doormanTimeout": <expiration time for CSR request in minutes>,
    "autoApproval": <is all incoming requests are automatically accepted or requires the approval by Administrator (true/false) >,
    "adminNotificationEmail": "<mail address which new CSR request's notification is sent to>",
    "nmsEmail": "<mail address used by Deon Doorman to send the CSR request status notifications>",
    "localStorage": "<filename where the network information is stored>"
    "eventHorizon": "<expected frequency of the node info publication>",
    "validityPeriodOfCRLList": "<validity period for issued CRL list>",
    "repo": {
        "host": "<Legal Identity of the BNO Node for cordapps distribution >"
    },
    "networkParametersUpdateDelay": "<delay, given to the nodes to accept the network parameter update>",
    "currentParameters": "<hash for the current network parameters>"
}
```


*Example:*
```json
{
    "doormanControlled": true,
    "doormanTimeout": 30,
    "autoApproval": true,
    "adminNotificationEmail": "konstantin.assanov@deondigital.com",
    "nmsEmail": "deondigit@gmail.com",
    "localStorage": ".db"
    "eventHorizon": "PT13M",
    "validityPeriodOfCRLList": "PT12H",
    "repo": {
        "host": "O=BNO Repository,L=Zurich,C=CH"
    },
    "networkParametersUpdateDelay": "PT30M",
    "currentParameters": "EE2732071A59BBF83880803877B36C03C3DB8956C72C7DDEF55DF0AE98237747"
}
```

### 1.3. Activate/de-activate the Auto-approval mode

If the 'auto-approval' mode is activated (true), all valid incoming CSR requests to join the Community Zone are accepted automatically
without need of the Administrator intervention.

If not(false), the CSR Request stays PENDING until the Administrator has accepted (or rejected) the request explicitly,
or the delay for the request processing passed (request is put in the EXPIRED state).

**POST** http://\<*NMS Address*\>:8080/admin/api/auto-approval/**\<*isAutoApproval*\>**

**\<*isAutoApproval*\>** - 'true' for the auto-approval mode; otherwise - false.

***Returns:*** new Auto-Approval mode (true/false)

## 2. CSR Requests Acceptance procedure requests

### 2.1. List all CSR Requests to join the Community Zone

**GET** http://\<*NMS Address*\>:8080/requests

***Returns:*** Array of the CSR Request Status descriptions.<br>
Each entry is:
```json
{
        "id": <Unique CSR Request Identifier>,
        "rdn": {
            "c": "<country code>",
            "l": "<locality code>",
            "o": "<Organization name>"
        },
        "email": "<Node owner email address>",
        "status": "<status>",
        "timeCreated": <Moment of receiving of the request>,
        "timeResolved": <Moment when the request was processed>,
        "timeDeleted": <Moment of removal of CSR request if ever; else null>
}
```

*Example:*
```json
    {
        "id": "8934ca2a-0dd4-4918-aa8b-44d7c176dc95",
        "rdn": {
            "c": "CH",
            "l": "Zurich",
            "o": "NonValidatingNotary"
        },
        "email": "konstantin.assanov@deondigital.com",
        "status": "AUTHORIZED",
        "timeCreated": 1551112158924,
        "timeResolved": 1551112242726,
        "timeDeleted": null
    }
```

### 2.2. Authorize or reject the pending CSR Request

**POST** http://\<*NMS Address*\>:8080/requests/**\<*CSR request ID*\>**/**\<*authorize cmd = (true/false)*\>**

**\<*CSR Request ID*\>** - Unique CSR Request Identifier from the list of CSR Requests

**\<*authorize cmd = (true/false)*\>** - 'true' or 'false'.
'true' is to accept the CSR request and allow to join the Community Zone
'false' is to reject the CSR request.

***Returns:*** the new CSR Request Status description.

## 3. Registered nodes management

### 3.1 List the nodes in Network Map (publicly accessible, no need from Administrator authentication)

**GET** http://\<*NMS Address*\>:8080/admin/api/nodes

***Returns:*** Array of the Nodes descriptions. <br>
Each entry is:
```json
    {
        "nodeKey":"<Node Key>",
        "addresses":<addresses>,
        "parties":<parties>,
        "platformVersion":<platform version>
    }
```

*Example:*
```json
    {
        "nodeKey":"0217099928C12F6F9DC611F287D9CC288D038FCAF847ADD5EA962D314407C266",
        "addresses":[{"host":"68.183.64.58","port":8001}],
        "parties":[{"name":"O=NonValidatingNotary, L=Zurich, C=CH",
                    "key":"GfHq2tTVk9z4eXgyNJTd4vyRfnas8jrczuv1W11bGKRajEBSoCy6DFiZtbU6"}],
        "platformVersion":4
    }
```


### 3.2. Delete a Node from Network Map

**DELETE** http://\<*NMS Address*\>:8080/admin/api/nodes/**\<*Node Key*\>**

**\<*Node Key*\>** - Node Key of the node to delete, received from the 'List the nodes' request

***Returns:*** void.

### 3.3. Delete all Nodes (including notaries) from Network Map

***DELETE*** http://\<*NMS Address*\>/admin/api/nodes-all

***Returns:*** void.

### 3.4. List issued certificates

**GET** http://\<*NMS Address*\>:8080/admin/api/certificates

***Returns:*** Array of the Node Certificates descriptions. <br>
Each entry is:
```json
    {
        "legalName":"<Node Legal Name>",
        "data": {
            "serial":<serial>,
            "revoked":<true/false>,
            "dateIssued": <date of certificate issue>,
            "dateRevoked": <date of certificate revocation>,
            "dateReinstated": <date of certificate reinstation>
        }
    }
```

### 3.5. Revoke certificate

**POST** http://\<*NMS Address*\>:8080/admin/api/certificates/**\<*Node Key*\>**/revoke

***Returns:*** Revoked certificate's serial number <br>

### 3.6. Reinstate certificate

**POST** http://\<*NMS Address*\>:8080/admin/api/certificates/**\<*Node Key*\>**/reinstate

***Returns:*** Reinstated certificate's serial number <br>

## 4. Cordapps management

### 4.1. List allowed cordapps

**GET** http://\<*NMS Address*\>:8080/admin/api/cordapps

***Returns:*** List of available cordapps

### 4.2. Add to list of allowed cordapps

**PUT** http://\<*NMS Address*\>:8080/admin/api/cordapps

***Body:*** *"\<group id\>:\<artifact id\>"*<br>
        *Content/Type: text/plain*

### 4.3. Remove of a cordapp from the list

**DEL** http://\<*NMS Address*\>:8080/admin/api/cordapps

***Body:*** *"\<group id\>:\<artifact id\>"*

## 5. Network Map Service Deployment

NMS can be deployed on a simple Ubuntu Droplet. Java have to be installed.
NMS can be launched with the following command line:

```
nohup java -Droot-ca-name="<Root CA Legal Identity>" -Dtls=false -Dpermissioned=true -Ddoorman-timeout=4320 -Dnms-email=<nms-email> -Dnms-pwd=<nms-email-pwd> -Devent-horizon=PT10M -Dparam-update-delay=PT30M -jar network-map-service.jar &
```
where
* permissioned(true/false) - true, if the Doorman should be activated;
* doorman-timeout - period of time in minutes when the CSR request should be processed;
* nms-email - email used by NMS to send the notifcations to user;
* nms-pwd - email pwd used by NMS;
* event-horizon - how often each node should push its Node Info to NMS to be considered alive;
* crl-validity - how long CRL list should be considered as valid;
* crl-localhost - if launched locally and the CRL list should be retrieved from localhost;
* param-update-delay - delay, given to the nodes to accept the network parameter update.

## 6. Network Parameters Update Acknowledgements

### 6.1. List of acknowledgements received for the network parameters update

**GET** http://\<*NMS Address*\>:8080/network-map/acknowledgements

***Returns:*** List of received acknowledgements per parameters update

## 7. Network Map Service deployment procedure

**Step 1.** Deploy Network Map Service<br>
**Step 2.** Deploy Notary Node<br>
**Step 3.** Deploy BNO Node as a hoster for *cordapps*