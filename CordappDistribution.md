Cordapp distribution solution
=============================

# 1. General Design

The purpose of the solution is to allow to the Node owner to install its cordapps
in a way that would be controlled by Business Network Operator that defines
what are the cordapps that could be installed and what are the sources of these cordapps.
The Node owner initiates the actual installation of the Cordapps, and manages the migration
to the newer version of cordapps.

## The principles:

1. BNO Node is a Unique Point Of Access to the Maven repository
that contains the cordapps artifacts. This BNO node only can access the repository outside
of the network.<br><br>All member nodes necessarily have to address the BNO node to load the cordapp artifacts
they need.

2. BNO Node communicates with the external Maven Repository - gains the authenticated access
to the Maven repository and downloads the artifacts on-demand.

3. Member nodes communicate with the BNO node by Corda Flows. The artifact is transmitted
with the Corda Transport flow.

4. Member node provides the Cordapp Installer API to the Node owner Client application in order
that the Client can initiate the actual installation of the artifacts available through the BNO node.
The downloaded cordapp is directly copied into the dedicated 'cordapp' directory.

# 2. Cordapp Installer Client API

The class to access the functionalities of the cordapp installer is ArtifactInstaller
with the following methods:

1. **versions(:group id, :artifact id)**
<br> - Obtain the list of available version for the given artifact;<br>

    Throws *UnauthorizedArtifactException* if the artifact is prohibited by BNO.

2. **sha1(:group id, :artifact id, :version)**<br>
**md5(:group id, :artifact id, :version)**
<br> - Download the content of the '.sha1' and '.md5' of the given artifact, and
    returns it as a string.

    Throws *UnauthorizedArtifactException* if the artifact is prohibited by BNO.

3. **install(:group id, :artifact id, :version)**
<br> - Download and install into the dedicated 'cordapp' directory
the artifact. Once this method is executed the given cordapp is installed,
and the restart of the Corda node is required only;<br>

    Returns the SHA-1 hash code of the downloaded file.<br>

    Throws *UnauthorizedArtifactException* if the artifact is prohibited by BNO.

4. **repositories()**
<br> - List the defined repositories (BNO Node Name, repository Name).

5. **artifacts(:bno node name, :repository name)**
<br> - List all available artifacts;

    Returns the list of available artifacts in the "\<groupId\>:\<artifactId\>" string form.

    Throws *UnknownHostException* if the **:bno node** can't be found.

6. **remove(:artifact id, :version)**
<br> - remove a previously installed version of the artifact.

    Returns *true* if deleted successfully; *false* otherwise.

7. **removeAll(:artifact id, :exclude)**
<br> - remove all the version of the given artifact, if **:exclude** version is omitted.<br>
<br> - if **:exclude** is defined, all versions, except the **:exclude** version, of the given artifact will be removed.

    Returns the list of removed artifact versions.

# 3. Configuration

This is a minimal configuration, and it's preconditioned by the requirements
of the *cordapp-updates-distribution* solution.

\[*The given configuration is a subject to modifications.*\]

### 3.1. BNO Node configuration

**/cordapps/config/corda-updates-app.conf**:
```
# list of the Maven repositories to request
repositories {
    <repository name #1> = "<repository link>"
}
# the credentials to access the repository
auth {
    <repository name #1> = {
        username = "<username>"
        password = "<password>"
        cordapps = ["<group id1>:<artifact id1>", "<group id2>:<artifact id2>"]
    }
}
```

*Example:*
```
repositories {
    deonRepo = "http://nexus.deondigital.com/repository/deon/"
}
auth {
    deonRepo = {
        username = "deon-dev"
        password = "deon-dev-password"
        cordapps = ["com.axedras:axedras-resource-flows", "com.axedras:axedras-resource-states"]
    }
}
 ```

### 3.2. Member Node Configuration

**/cordapps/config/corda-updates-app.conf**:

```
# list of cordapps sources with the references to remote repository URLs
cordappSources = [
    {
        remoteRepoUrl = "<Remote Repository URL>"
        cordapps = ["<group id1>:<artifact id1>", "<group id2>:<artifact id2>"]
    }
]
```

*Example:*
```
cordappSources = [
    {
        remoteRepoUrl = "corda:O=BNO,L=Zurich,C=CH/deonRepo"
        cordapps = ["com.axedras:axedras-resource-flows", "com.axedras:axedras-resource-states"]
    }
]
```

### 3.3. Distribution cordapps to install for the distribution service:

Cordapps | BNO Node | Member Node |Orig/Deon
---------|:--------:|:-----------:|:--------:
corda-updates-core-\<version\>.jar | &#10004; | | original |
corda-updates-transport-\<version\>.jar | &#10004; | &#10004; | deon |
