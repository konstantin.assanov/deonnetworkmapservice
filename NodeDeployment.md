# Node Deployment Guide

There is also a simple procedure to deploy a corda node on Digital Ocean in a way that it could try to join automatically the network.

It consists of 2 major steps: <br>
**Step 1.** Configure the Digital Ocean droplet from the specially provided Image, <br>
**Step 2.** Get authorization from Deon Doorman on joining the network.

## Step 1. Droplet with your Corda Node creation procedure

1. Push the green *"Create"* droplet button at your Digital Ocean space.
2. *Choose an image -> Distributions**: Ubuntu
3. *Choose a Plan* of your choice (suggestion: 2GB has to be sufficient)
4. *Choose a data-center region* of your choice
5. *Select additional options*: <br>

***User Data*** (sic!) must be defined following one of these 2 models - (A) from clean Ubuntu droplet or (B) from java pre-installed Ubuntu droplet:

**A. If from clean Ubuntu droplet:**
```script
#cloud-config
runcmd:
 - [ curl, "http://<nms-server-ip:port>/node-script/[v/n]", -o, /root/node.sh]
 - chmod +x /root/node.sh
 - [ /root/node.sh, "<Legal identity>", "<Your email>", "<RPC user>", "<RPC password>"]
 ```
where:<br>
-    /node-script        - regular Corda node;<br>
-    /node-script/v      - Validating Notary Corda node;<br>
-    /node-script/n      - Non-validating Notary Corda node.<br>


_Example: Ordinary Node_
```script
#cloud-config
runcmd:
 - [ curl, "http://157.230.18.95:8080/node-script", -o, /root/node.sh]
 - chmod +x /root/node.sh
 - [ /root/node.sh, "O=GreatDay, L=Zurich, C=CH", "konstantin.assanov@deondigital.com", "usr", "pwd"]
```

***NB: Save the information you submitted into User Data section, as it will be usable for further communication with your Node via, for example, ResourceClient or CordAppClient.***

**B. If from java pre-installed Ubuntu droplet:**<br>
(Go to *Choose an Image > Snapshots*. Select the image "internal-deon-corda4.0-RC04-node-ubuntu-v3.5")
```script
#cloud-config
runcmd:
 - [ curl, "http://<nms-server-ip:port>/node-script-int/[v/n]", -o, /root/node.sh]
 - chmod +x /root/node.sh
 - [ /root/node.sh, "<Legal identity>", "<Your email>", "<RPC user>", "<RPC password>"]
```
where:<br>
-    /node-script-int    - regular Corda node;<br>
-    /node-script-int/v  - Validating Notary Corda node;<br>
-    /node-script-int/n  - Non-validating Notary Corda node.<br>

_Example: Ordinary Node_
```script
#cloud-config
runcmd:
 - [ curl, "http://157.230.18.95:8080/node-script-int", -o, /root/node.sh]
 - chmod +x /root/node.sh
 - [ /root/node.sh, "O=GreatDay, L=Zurich, C=CH", "konstantin.assanov@deondigital.com", "usr", "pwd"]
```

***NB: Save the information you submitted into User Data section, as it will be usable for further communication with your Node via, for example, ResourceClient or CordAppClient.***

6. Optional: Then *Add your SSH key* to be able to ssh your droplet (in case of need)
7. Choose your hostname.
8. Finish by "Create" button.
9. Once the droplet is instantiated, a request to Deon Doorman is sent.

## Step 2. Node joining the network process

The process of the registration and launch of your Corda node is automatic.
The evolution of this can be followed by the reception of the mail notification on the state of the network join request:

**Notification email 1.** As soon as the registration request is received by Network Map Service (Deon Doorman), you receive
a notification mail that your request either received successfully, or considered as invalid. If the request
has been accepted by Deon Doorman, you should wait for the explicit approval (authorization) by Deon Doorman.

**Notification email 2.** As soon as Deon Doorman either authorizes, or rejects the Nodes request (to join the network), you receive
a notification mail with AUTHORIZED or REJECTED status. If no action was done by Deon Doorman in the due time, you will
receive the EXPIRED request notification mail.

Once Authorization confirmation is received, it means that your node is operational and accepted to the network.

## Corda Node usage parameters

Once your Corda Node is running you can access it via RPC service with the following parameters:

1. RPC IP Address: the IP address of your droplet
2. RPC service port: 8011
3. RPC Credentials: \<RPC user\>/\<RPC password\>, as defined in User Data section

## Node.sh Access

For Empty Ubuntu Droplet:
1. Regular node: GET /node-script
2. Notary node: <br>
 a. Validating: GET /node-script/v <br>
 b. Non-Validating: GET /node-script/n

For Pre-prepared Ubuntu Droplet:
 1. Regular node: GET /node-script-int
 2. Notary node: <br>
  a. Validating: GET /node-script-int/v <br>
  b. Non-Validating: GET /node-script-int/n

## BNO Node deployment

**BNO Node deployment from clean Ubuntu droplet:**
```script
#cloud-config
runcmd:
 - [ curl, "http://<nms-server-ip:port>/node-script-bno", -o, /root/node.sh]
 - chmod +x /root/node.sh
 - [ /root/node.sh, "<Legal identity>", "<Your email>", "<RPC user>", "<RPC password>"]
 ```