# Deon Network Map Service

The Corda Network is permissioned, and the management of the different 
aspects of the network is the subject of the Business Network Operator policy.

The management covers the following aspects:
1. Administrator's capabilities;
2. Network Parameters update;
3. Corda Node deployment;
4. Cordapps distribution to the corda nodes.

## 1. Deon Network Map Service Administrator's Guide

The Administrator of the Network manages the joining and removal of the nodes
from the network. It controls all different aspects of the network life cycle. 

The Administrator's Guide describes the REST endpoints to manage the network [Admin.md](Admin.md). 


## 2. Processing the Network Parameters update Guide

The "Network Parameters" groups the parameters that should be shared
by all nodes of the network for the proper functioning. Thus,
the network parameters update is a crucial operation, initiated
by the network map service, but managed through the acceptance by
each individual node.

The Network Parameters update is defined at [Acknowledgements.md](Acknowledgements.md).

## 3. Node Deployment procedure 

There is a simple procedure to create a Digital Ocean droplet that
hosts pre-configured Corda Node and joins the corda nodes network.

This Corda Node Droplet creation will configure the hosted Corda node, issue
the network join request, and launch the Corda node automatically.

After such deployment the Corda Node becomes fully operational.

The Node Deployment procedure is described at [NodeDeployment.md](NodeDeployment.md).

The next step to do would be initiate the installation of the needed cordapps
on the node. This is managed with the **Cordapps Distribution Solution**.    

## 4. Cordapps Distribution Solution

The purpose of the solution is to allow to the Node owner to install its cordapps
in a way that would be still under the control of Business Network Operator. 

This solution is described at [CordappDistribution.md](CordappDistribution.md).